# ProjectName Heading (JIRA NAME) [![build status](https://gitlab.msu.edu/HR_JAVA/EdAssist/badges/master/build.svg)](https://gitlab.msu.edu/HR_JAVA/EdAssist/commits/master)

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi viverra gravida efficitur. Mauris eu faucibus nibh. Nulla facilisi. Nullam bibendum rutrum mollis. Curabitur ac nibh eu nisi condimentum fringilla quis vel arcu. Integer et nunc et eros hendrerit venenatis non nec turpis. Praesent vel pharetra tortor.

Fusce ut arcu nec risus tincidunt pulvinar ut vel felis. Fusce congue nisl ut arcu commodo laoreet. Donec felis lacus, gravida at dui et, maximus ultricies erat. Aliquam laoreet faucibus sem, non suscipit felis eleifend tincidunt. Aliquam maximus commodo venenatis. Aenean sit amet augue ac felis dignissim porta a vel ante. Duis dignissim nunc et justo molestie feugiat. Vivamus aliquet nulla vitae diam consectetur, sed faucibus nisl consequat.

## Repository Structure

The Project Name main source repository includes the following components:

* [Environment keys] (env/readme.md)
* [Twitter API Keys] (twitter/readme.md)
* [Google API key] (google/readme.md)

## Development


#### Prerequisites

* Java 1.8
* Third Party Lib v6.3
* git client

#### Getting Started

Tecenas luctus pellentesque metus quis pellentesque. Ut ut sagittis enim, ac interdum sem. Fusce faucibus augue ornare tellus sollicitudin gravida at vel lorem. Etiam aliquam velit eget leo accumsan tempus.

```bash

./git clone git@gitlab.msu.edu:HR_JAVA/EdAssist.git

chmod +x gradlew

./gradlew edassist-web:war -Pbuildtype=<enviroment name i.e. prod>

```

> (Notes) Tecenas luctus pellentesque metus quis pellentesque. Ut ut sagittis enim, ac interdum sem. Fusce faucibus augue ornare tellus sollicitudin gravida at vel lorem. Etiam aliquam velit eget leo accumsan tempus.


#### Usage

#### Setting the proxy server settings

```bash
export set JAVA_OPTS="-Dhttps.proxyHost=netman.ais.local -Dhttps.proxyPort=3128"
```

#### Executing
```bash
./MainApp -p "input.properties"
```

## Deployment

#### Overview

```mermaid
graph LR;
  Build --> Test;
  Test  --> Deploy_Test;
  Deploy_Test --> Deploy_Qual;
  Deploy_Qual --> Deploy_Prod;
```

#### Enviroments

| URL          | Server | Status | 
| :----------- | :------: | ------------: |
| https://xxxx.itservices.msu.edu | usysprd1a[1:2].itservices.msu.edu   |  Deployed |
| https://xxxx.qual.itservices.msu.edu | usysqa1a[1:2].itservices.msu.edu   | Deployed        |
| https://xxxx.test.itservices.msu.edu | usystst1a1.itservices.msu.edu   | Deployed        |
| https://localhost:8080/edassist          | N/A   | N/A        |



## Troubleshooting

This area can be used to document any troubleshooting tips. Maybe you need to:

Ensure port 9990 is open
Run as administrator
