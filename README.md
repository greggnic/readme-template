# Application Name

This should be a brief description of the application. What is it's purpose?

# Features

### Feature one

Feature description should go here

  - Feature highlight 1
  - Feature highlight 2

# Installation

### Step one

This should be a detailed description of the first step. You can have a list of tasks:

- Download software
- Configure software
- etc

And you can include code snippets:

```
cp source-dir destination-dir
```

# Running

These are special instructions for running the application. Maybe the application needs to be ran with special arguments:

```
java -jar -Dspring.profiles.active=h2
```

# Troubleshooting

This area can be used to document any troubleshooting tips. Maybe you need to:

- Ensure port 9990 is open
- Run as administrator
